using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace PruebasAutomaticas.Tests
{
    [TestClass]
    public class TransferenciasTest
    {
        [TestMethod]
        public void TransferenciaEntreCuentaConFondosInsuficientesArrojaUnError()
        {
            //Preparación
            Exception exprectedException = null;
            Cuenta origen = new Cuenta() { Fondos = 0 };
            Cuenta destino = new Cuenta() { Fondos = 0};
            decimal montoATransferir = 5m;
            var mock = new Mock<IServicioValidacionesDeTransferencias>();
            string mensajeError = "mensaje de error";
            mock.Setup(x => x.RealizarValidaciones(origen, destino, montoATransferir))
                .Returns(mensajeError);
     
            var servicio = new ServicioDeTransferencias(mock.Object);

            //Prueba
            try
            {
                servicio.TransferirEntreCuentas(origen, destino, montoATransferir);
                Assert.Fail("Un error debió ser arrojado");

            }
            catch (Exception ex)
            {

                exprectedException = ex;
            }

            //Verificación
            Assert.IsTrue(exprectedException is ApplicationException);
            Assert.AreEqual(mensajeError, exprectedException.Message);
        }

        [TestMethod]
        public void TransferenciaValidaEditaLosFondosDeLasCuentas()
        {
            //Preparación
            Cuenta origen = new Cuenta() { Fondos = 10 };
            Cuenta destino = new Cuenta() { Fondos = 5 };
            decimal montoATransferir = 7m;
            var mock = new Mock<IServicioValidacionesDeTransferencias>();

            mock.Setup(x => x.RealizarValidaciones(origen, destino, montoATransferir))
                .Returns(string.Empty);

            var servicio = new ServicioDeTransferencias(mock.Object);

            //Prueba
            servicio.TransferirEntreCuentas(origen, destino, montoATransferir);

            //Verificaión 
            Assert.AreEqual(3, origen.Fondos);
            Assert.AreEqual(12, destino.Fondos);
        }
    }
}
